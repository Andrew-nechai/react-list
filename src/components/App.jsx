import React from 'react';
import TitleTodo from './TitleTodo.jsx'
import InputTodo from './InputTodo.jsx'
import Todolist from './Todolist.jsx'
// import Modal from './Modal.jsx'
import AdditInfo from './AdditInfo.jsx'
import ButtonsBlock from './ButtonsBlock.jsx'
import {Route, Switch} from 'react-router-dom'
import {changeMass, changeButMass, removeSelectItems} from './redux/actions.jsx'
import {connect} from 'react-redux'


class App extends React.Component {
  constructor(props) {
      super(props);
      this.title = "TODOLIST";
      this.placeholdertodo = "Добавить задачу";
      this.count_check_tasks = this.props.mass.filter(task => task.completed == false).length;
      this.parent = this.props.parent;
      this.onChange = this.onChange.bind(this);
      this.handleKeyPress = this.handleKeyPress.bind(this);
      this.deleteTask = this.deleteTask.bind(this);
      this.getRandomId = this.getRandomId.bind(this);
      this.changeCountTasks = this.changeCountTasks.bind(this);
      this.selectTask = this.selectTask.bind(this);
      this.changeTasksState = this.changeTasksState.bind(this);
      this.changeButtonsState = this.changeButtonsState.bind(this);
      this.emptyTasksMas = this.emptyTasksMas.bind(this);
      this.isButtonKlicked = this.isButtonKlicked.bind(this);
      this.isSelectedItems = this.isSelectedItems.bind(this);
      this.changeCountUncheck = this.changeCountUncheck.bind(this);
      this.saveInLocalStorage = this.saveInLocalStorage.bind(this);
      this.resetToolButtonsStyle = this.resetToolButtonsStyle.bind(this);
      this.resetSelectTasksStyle = this.resetSelectTasksStyle.bind(this);

      this.deleteSelectedDeals = this.deleteSelectedDeals.bind(this);


      // this.props = {
      //   mass: [],
      //   buttons: [
      //     {name: "all", klicked: false},
      //     {name: "active", klicked: false},
      //     {name: "complete", klicked: false}
      //   ]
      // };
  }

  componentDidMount(){
    console.log("Update");
    // let newmas = JSON.parse(localStorage.getItem(this.parent)) || [];
    // let object = changeMass(newmas);
    // this.props.changeMass(object);
    // this.count_check_tasks = this.props.mass.filter(task => task.completed == false).length;
  } 
  
  saveInLocalStorage(mass) {
    localStorage.setItem(this.parent, JSON.stringify(mass));
  }

  getRandomId() {
    let mas = this.props.mass.map(item => item.id); 
    let otv;
    while (true) {
      otv = Math.floor(Math.random() * (30000 - 1) + 1);
      if (!mas.includes(otv)) {
        break;
      }
    }
    return otv;
  }

  handleKeyPress(event) {
    let mas = this.props.mass.slice(0);
    if (event.currentTarget.value === "") {
      return;
    }
    
    if (event.keyCode == 13) { 
        this.count_check_tasks++;  
        let generated_id = this.getRandomId(); 
        mas.unshift({id: generated_id, completed: false, text: event.currentTarget.value, selected: false, href: generated_id});
        event.currentTarget.value = "";
        // this.saveInLocalStorage(mas);
        this.resetToolButtonsStyle();
        this.saveInLocalStorage(mas);
        this.resetSelectTasksStyle();
        this.props.changeMass(changeMass(mas));
    }
  }

  changeCountTasks(task) {
    if (task.completed) {
      this.count_check_tasks--;
    } 
    else {
      this.count_check_tasks++;
    }
  }

  onChange(id) {
    let mas = this.props.mass.slice(0);
    mas.map(task => {
      if (task.id == id) {
        task.completed = !task.completed;
        if (task.completed) {
          this.count_check_tasks--;
        } 
        else {
          this.count_check_tasks++;
        }
      }
    });
    this.saveInLocalStorage(mas);
    this.props.changeMass(changeMass(mas));
  }

  changeTasksState(){
    let newmass = this.props.mass.slice(0);

    let count = 0;
    
    newmass.forEach(element => {
        if (!element.completed) {
            count++;
        }
    });

    this.changeCountUncheck(count);
    // this.saveInLocalStorage(newmass);
    this.props.removeSelectItems(removeSelectItems(newmass)); 
  }

  changeButtonsState(newbuttons){
    // this.setState({
    //   buttons: newbuttons
    // })
    // let buts_mass = this.props.buttons;
    this.props.changeButMass(changeButMass(newbuttons));
    // this.props.resetButtonsStyle(resetButsStyles(newbuttons));
  }

  deleteTask(id, check) {
    let mas = this.props.mass.filter((task) => task.id !== id);
    
    if (!check) {
      this.count_check_tasks--;
    }
    
    this.saveInLocalStorage(mas);
    this.props.changeMass(changeMass(mas));
  }

  selectTask(id) {
    let mas = this.props.mass.slice(0);
    this.props.mass.map(task => { 
      if (task.id == id) {
        task.selected = !task.selected;
      }
    });
    this.props.changeMass(changeMass(mas));
    // this.changeTasksState(mas);
    // this.setState({
    //   mass: mas
    // });
  }

  resetToolButtonsStyle() {
    let buttons_mas = this.props.buttons.map(button => {button.klicked = false; return button});
    console.log("Buttons in state: ", this.props.buttons, "Buttons in mass: ", buttons_mas);
    this.props.changeButMass(changeMass(buttons_mas));

    // this.setState({
    //   buttons: buttons_mas.map(button => {
    //     button.klicked = false;
    //     return button;
    //   })
    // });
  }

  resetSelectTasksStyle() {
    let tasks_mas = this.props.mass;
     tasks_mas.map(task => {
        task.selected = false;
      })
    this.props.changeMass(changeMass(tasks_mas));
  }    

  emptyTasksMas() {
    if (this.props.mass.length) {
      return true
    }
    return false;
  } 

  isButtonKlicked() {
    let otv = false;
    this.props.buttons.forEach(element => {
      if (element.klicked) {
        otv = true;
      }
    });
    return otv;
  }


  isSelectedItems() {
    let otv = false;
    this.props.mass.map((element) => {
      if (element.selected) {
        otv = true;
      }
    });
    return otv;
  }

  changeCountUncheck(count) {
    this.count_check_tasks = count;
  }

  deleteSelectedDeals(tasks) {
    console.log("WTF")
    this.props.changeMass(changeMass(tasks));
    this.saveInLocalStorage(tasks);
  }

  render() {
    console.log("State: ", this.props)
    // console.log("Render Mass:", this.props.mass.slice(0));
    return (
        <div>
          <Route path="/" exact render={() => 
            <section className="todolist">
                <TitleTodo text = {this.title}/>
                <InputTodo keypress = {this.handleKeyPress}/>
                <Todolist massiv = {this.props.mass} 
                          onChange = {this.onChange} 
                          deleteTask = {this.deleteTask} 
                          onSelect = {this.selectTask}
                          resetToolButtonsStyle = {this.resetToolButtonsStyle}   
                          resetSelectTasksStyle = {this.resetSelectTasksStyle}
                          isButtonKlicked = {this.isButtonKlicked}  
                />
                <div className="todolist__toolbar">
                    <div className="count-uncheck-deals">
                        { 
                          "Количество невыполненных дел: " + this.count_check_tasks
                        }
                    </div>
                    <ButtonsBlock changeParentTasksState = {this.changeTasksState} 
                                  deleteDeals = {this.deleteSelectedDeals}
                                  parentstate = {this.props} 
                                  buttonsmass = {this.props.buttons}
                                  changeParentButtonsState = {this.changeButtonsState} 
                                  resetToolButtonsStyle = {this.resetToolButtonsStyle}
                                  resetSelectTasksStyle = {this.resetSelectTasksStyle}
                                  isSelectedItems = {this.isSelectedItems}
                                  emptyTasksMas = {this.emptyTasksMas}
                                  changeCountUncheck = {this.changeCountUncheck}
                                  saveInLocalStorage = {this.saveInLocalStorage}
                                  />
                </div>
              </section>} />
          <Route exact path="/deal/:id" component={AdditInfo}/>
      </div>
      );
  }
}

function mapStateToProps(state) {
  return {
      mass: state.mass,
      buttons: state.buttons
  }
}

function mapDispatchProps(dispatch) {
  return {
    changeMass: (obj) => dispatch(obj),
    changeButMass: (obj) => dispatch(obj),
    removeSelectItems: (obj) => dispatch(obj)
  } 
}

export default connect(mapStateToProps, mapDispatchProps)(App);