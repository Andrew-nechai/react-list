import React from 'react';

function InputTodo(props) {
    return (
        <div className="input-container">
            <input type="text" className="todolist__input" placeholder="Добавить задачу" onKeyUp={props.keypress}/>
        </div>
    );
}

export default InputTodo;