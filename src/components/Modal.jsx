import React from 'react';
import CloseButton from './img/close-modal.svg'
import {connect} from 'react-redux'
import { CSSTransition } from 'react-transition-group'
import {showModal} from './redux/actions.jsx'
import { changeMass } from './redux/actions.jsx'

class Modal extends React.Component {
    constructor(props) {
        super(props);
        this.modalAnimationName = "";
        this.wrapperModalAnimationName = "";
        this.closeModal = this.closeModal.bind(this);
        this.changeAdditInfo = this.changeAdditInfo.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.closeModall = this.closeModall.bind(this);
        this.saveInLocalStorage = this.saveInLocalStorage.bind(this);
        this.state = {value: ''};
    }   

    saveInLocalStorage(mass) {
        localStorage.setItem('root', JSON.stringify(mass));
      }

    closeModal(e) {
        e.stopPropagation();
        this.props.showModal(false);
    }

    closeModall() {
        this.props.showModal(false);
    }

    changeAdditInfo(e) {
        e.stopPropagation();
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        let text = this.state.value;
        let id = this.id;
        let mass = this.props.mass.map((item, index) => {
            if (item.id == this.props.id) {
                if (this.props.type == "aim") {
                    item.text = text;
                }
                else {
                    if (this.props.type == "details") {
                        item.details = text;
                    }
                    else {
                        item.location = text;
                    }
                }
            }
            return item;
        });
        this.saveInLocalStorage(mass);
        this.props.parentSetState({
            whatNeedToChange: ""
        });
        this.closeModall();
    }

    render() {
        if (this.props.isShowModal) {
            return (
                <CSSTransition
                    in={true}
                    classNames="bgModal"
                    timeout={1000}
                >
                <div className="change_deal_block">
                    <div className="change_deal_block__wrapper">
                        <CSSTransition
                            in={this.props.isShowModal}
                            classNames="modal"
                            timeout={1000}
                        >
                        <div className="change_deal">
                            <div className="change_deal__title">
                                <div className="change_deal__title_text">{"Введите данные"}</div>
                                <div className="change_deal__exit" onClick={(e) => {this.closeModal(e)}}><img src={CloseButton} alt="X"/></div>
                            </div> 
                            
                            <form onSubmit={this.handleSubmit}>
                                <div className="change_deal__input"><input type="text" value={this.state.value} onChange={this.handleChange}  placeholder="Введите значение"/></div>
                                <div className="change_deal__button"><button type="submit" onClick={this.changeAdditInfo}>{"Submit"}</button></div>
                            </form>
                        </div>
                        </CSSTransition>
                    </div>
                </div>
                </CSSTransition>
            );
        } 
        else {
            return (<span></span>);
        }
    }
}

function mapStateToProps(state) {
    return {
        isShowModal: state.showModal,
        mass: state.mass
    }   
}

function mapDispatchProps(dispatch) {
    return {
        showModal: bool => dispatch(showModal(bool)),
        changeMass: (mass) => dispatch(changeMass(mass))
    }
}

export default connect(mapStateToProps, mapDispatchProps)(Modal);