import { CHANGEMASS, CHANGEBUTMASS, REMOVESELECTITEMS, SHOWMODAL } from "./types";
         
export function changeMass(mass) {
    return {
        type: CHANGEMASS,
        mass: mass  
    }
}

export function changeButMass(mass) {
    return {
        type: CHANGEBUTMASS,
        buttons: mass  
    }
}

export function removeSelectItems(mass) {
return {
    type: REMOVESELECTITEMS,
        mass: mass
    }
}

export function showModal(bool) {
    return {
        type: SHOWMODAL,
        showModal: bool
    }
}

// export function addDeal(mass) {
//     return {
//         type: ADDDEAL,
//         mass: mass  
//     }
// }

// export function deleteDeal(mass) {
//     return {
//         type: DELETEDEAL,
//         mass: mass
//     }
// }

// export function checkboxChange(mass) {
//     return {
//         type: CHECKBOXCHANGE,
//         mass: mass
//     }
// }

// export function selectDeals(mass) {
//     return {
//         type: SELECTDEALS,
//         mass: mass
//     }
// }

// export function resetButsStyles(butmass) {
//     return {
//         type: RESETBUTTONS,
//         buttons: butmass
//     }
// }

// export function resetDealsStyles(mass) {
//     return {
//         type: RESETDEALSSTYLE,
//         mass: mass
//     }
// }