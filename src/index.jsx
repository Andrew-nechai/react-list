import React, { Component } from "react";
import ReactDOM from "react-dom";
// import './index.scss';
import App from './components/App.jsx';
import { createStore } from 'redux';
import {Provider} from 'react-redux';
import {BrowserRouter, Route} from 'react-router-dom'
import rootReduser from './components/redux/rootReduser.jsx';

let deals = JSON.parse(localStorage.getItem("root")) || [];

let store = createStore(rootReduser); 

// let todolist = (
//     BrowserRouter
//     <Route component={App}></Route>
//     <Provider store={store}>  
//         <App/>
//     </Provider>
// );


ReactDOM.render(
    <BrowserRouter>        
        <Provider store={store}>  
            <App parent="root"/>
        </Provider>
    </BrowserRouter>, 
document.getElementById('root'));